//! #Erros

use std::{
    fmt::{
        self,
        Display,
    },
    error::Error,
};

use crate::spin::State;


// Alias para `Result` com erros da biblioteca.
pub type SpinResult<T> = Result<T, SpinError>;


/// Enum contendo os erros da biblioteca
#[derive(Debug)]
pub enum SpinError {

    /// Acesso indevido de uma cadeia. O primeiro argumento
    /// é o índice do bit tentando ser acessado e o segundo
    /// é o tamanho da cadeia.
    OutOfBounds(u8, u8),

    /// Estado de sítio inválido. O primeiro argumento contem
    /// o inteiro correspondente ao estado do sítio da cadeia
    /// e o segundo contem o spin da cadeia. O terceiro é 
    /// simplesmente um valor para formatar a mensagem de erro.
    InvalidSiteState(State, &'static str, u8),

    /// Estado da cadeia ultrapassa o tamanho do visor de cadeia.
    /// O primeiro argumento é uma string contendo a representação
    /// binária do estado da cadeia e o segundo é o número de sítios
    /// do visor de cadeias usado.
    ChainOverflow(String, u8),

    /// Tentativa de criar uma cadeia que excede o tamanho do tipo
    /// usado para amazená-la. O primeiro contem o tamanho do visor
    /// tentando ser criado, o segundo, argumento é o nome do tipo
    /// usado para armazenar a cadeia e o terceiro, o número de bits
    /// que ele contem.
    StorageOverflow(u8, &'static str, u8),
}


impl Display for SpinError {

    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result
    {
        let m = match self {
            SpinError::OutOfBounds(index, size) =>
            {
                format!(
                    "Acesso indevido. Tentando acessar bit na posição {} \
                    de cadeia com {} bits.", index, size
                )
            }
            SpinError::InvalidSiteState(state, spin, padding) =>
            {
                format!(
                    "Estado de índice '{:0P$b}' é inválido para cadeias de spin {}",
                    state, spin, P = *padding as usize
                )
            }
            SpinError::ChainOverflow(chain_str, size) =>
            {
                format!(
                    "Cadeia no estado\ns = {}\né incompatível com visor com {} sítios.",
                    chain_str, size
                )
            }
            SpinError::StorageOverflow(size, storage_type, width) =>
            {
                format!(
                    "Visor requer {} bits mas o tipo {} suporta até {} bits.",
                    size, storage_type, width
                )
            }
        };

        write!(f, "{}", m)

    }
}


impl Error for SpinError {}
