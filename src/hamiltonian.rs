pub mod basis;

pub mod models;


use crate::spin_chain::storage::BitStorage;
use basis::Basis;

// Proposta
//
// A hamiltoniana deve ter uma interface do tipo:
//
// trait Hamiltonian {
//
// fn construct_block(basis) -> matrix;
//
// }
//
// Junto a isso, simetrias podem ser implementadas como:
//
// trait Symmetry {};
//
// trait Simetria-X: Symmetry {
//
//  fn basis(quantum_number: u8?) -> basis
//
//  }
//
//  Ou talvez possa ser direto no traço Symmetry e não sejam necessários
//  traços para simetrias particulares. Acho que é necessário várias
//  simetrias porque precisa de várias implementações. Uma hamiltoniana
//  pode ter mais de uma simetria.
//
//
//  Não sei como representar matrix de forma compatível com spin_chain.
//  Consultar bitvec para ver se dá alguma ideia. Colocar limites no
//  tamanho? u32 -> usize? Mudar bit_storage para [u8; L]? Implementações
//  de bigint também pode ser úteis.

/// Traço que caracteriza hamiltonianas. Requer apenas um método:
/// `construct_block` que constroi uma bloco de matriz dada uma
/// base, seja de todo o espaço ou de um setor de simetria.
pub trait Hamiltonian {
    
    fn construct_block<E: BitStorage>(basis: Basis<E>) -> Vec<Vec<f64>>;

}
