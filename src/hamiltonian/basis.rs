//! # Bases
//!
//! Define a `Basis` e sua interface. Usado para representar 
//! bases de vetores do espaço de Hilbert. Toda base é indexada
//! por um ou mais números quânticos que a classifica.

use crate::spin_chain::storage::BitStorage;


pub struct Basis<'a, E: BitStorage> {
    quantum_numbers: Vec<f64>,
    elements: &'a Vec<E>,
}
