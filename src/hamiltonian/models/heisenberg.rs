//! #Hamiltoniana de Heisenberg

// TODO
// [ ] - Documentação
// [ ] - Hamiltonianas deveria ser só módulos ou eu deveria ter estruturas
//       possivelmente vazias para representá-las? Com isso é possível 
//       implementar traços que podem ser úteis para classificá-las em
//       termos de simetrias...

// XXZ, S = 1/2
pub struct XXZ<const L: usize> {
    // Arrumar nomes melhores para esses campos
    j: f64,
    delta: f64,
}


impl<const L: usize> XXZ<L> {

    pub fn new(j: f64, delta: f64) -> Self {

        Self { j, delta }
    }


    // TODO
    // [ ] - É necessário garantir que L e E tenha tamanhos compatíveis?
    pub fn matrix_element(&self)
    {
        // L deve ser a dimensão do espaço de Hilbert, não da cadeia.
        let hamiltonian_matrix = [[0_f64; L]; L];
        println!("{}", L);
        // todo!();
    }

}
