//! <style>
//!
//! .top-doc h3 {
//!     border-bottom: 0;
//! }
//!
//! </style>
//! <div style="text-align: center;" align="center">
//! 
//! # `eXact Diagonalization`
//! 
//! ## Diagonalização exata de cadeias de spin <!-- omit in toc -->
//!
//! </div>
//!
//! 1. [Sumário](#sumário)
//! 1. [Introdução](#introdução)

pub mod error;
pub mod spin;
pub mod spin_chain;
// pub mod hamiltonian;

// A FAZER:
//
// [ ] - Melhorar a API. Eu acho que SpinChain e SpinChainView não são os melhores
//       nomes. Talvez SpinChain deva ir para ChainState (em oposição a SingleSiteState)
//       e SpinChainView para SpinChain. StateIterator funciona como uma espécie de
//       gerador de estados. Talvez algo similar a um Builder...


pub mod prelude {

    pub use crate::{
        spin_chain::{
            SpinChain,
            aliases::*,
            chain_view::{
                SpinChainView,
                OpenBoundary,
                PeriodicBoundary,
            },
            state_iterator::StateIterator,
        },
        spin::{
            SpinHalf,
            SpinOne,
        },
        // hamiltonian::models::heisenberg::XXZ,
    };
}

