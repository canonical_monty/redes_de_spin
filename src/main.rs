// Código baseado no artigo Guide to exact diagonalization study of
// quantum thermalization - Jung-Hoon Jung, Jae Dong Noh.


// use xd::prelude::*;
// use std::time::Instant;


fn main() 
{
    // println!("\n\n");
    // let chain = SpinHalfChain::<u8>::from_state(
    //     BoundaryCondition::Open,
    //     0b1101_0110
    // );

    // println!("{:?}", chain);
    // print!("\nSítios -> ");
    // for site in &chain
    // {
    //     print!("{:?} ", SpinHalf::try_from(site).unwrap()); 
    // }
    // println!("\n\n");
    // 
    // let chain = SpinOneChain::<u8>::from_state(
    //     BoundaryCondition::Open,
    //     0b10_01_01_00
    // );

    // println!("{:?}", chain);
    // print!("\nSítios -> ");
    // for site in &chain
    // {
    //     print!("{:?} ", SpinOne::try_from(site).unwrap()); 
    // }
    // println!("\n\n\n\n");

    // let chain_view = SpinChainView::<SpinHalf>::new(4, BoundaryCondition::Open);
    // let mut state_iterator = StateIterator::<u8, SpinHalf>::new(chain_view);
    // let half_iterator_start = Instant::now();
    // while let Some(chain) = state_iterator.next()
    // {
    //     println!("Estado: {:03} = {:08b}", chain.state(), chain.state());
    // }
    // let half_time = half_iterator_start.elapsed();

    // println!("\n\n\n");

    // let half_iterator_old_start = Instant::now();
    // for i in 0..2_u8.pow(4)
    // {
    //     let chain = SpinChain::<u8, SpinHalf>::from_state(BoundaryCondition::Open, i);
    //     println!("Estado: {:03} = {:08b}", chain.state(), chain.state());
    // }
    // let half_time_old = half_iterator_old_start.elapsed();

    // println!("\n\n\n");

    // let chain_view = SpinChainView::<SpinOne>::new(3, BoundaryCondition::Open);
    // let mut state_iterator = StateIterator::<u8, SpinOne>::new(chain_view);
    // let one_iterator_start = Instant::now();
    // while let Some(chain) = state_iterator.next()
    // {
    //     println!("Estado: {:03} = {:08b}", chain.state(), chain.state());
    // }
    // let one_time = one_iterator_start.elapsed();

   

    // println!("Tempos:\nSpin 1/2 = {:?}\t Velho = {:?}\nSpin 1 = {:?}", 
    //          half_time, half_time_old, one_time);

    // const SIZE: usize = 1 << 10;
    // let hamiltonian = XXZ::<SIZE>::new(1.0, 2.0);
    // // hamiltonian.matrix_element();
    // 
    // let row = vec![0_f64; SIZE];
    // let matrix = vec![row; SIZE];

    println!("Em progresso");
}
