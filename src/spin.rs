//! #Spin
//!
//! Define o traço `Spin` que caracteriza tipos de spins
//! distintos que uma cadeia pode ter.


// A FAZER:
// [ ] - Spin deveria exigir Debug + Display?
// [ ] - Revisar a documentação de Spin.



use std::convert::TryFrom;

use crate::error::{
    SpinError,
    SpinResult,
};


/// Alias para tratar os inteiros representando um estado de um sítio
/// de uma rede com dado valor de spin.
pub type State = u8;



/// Traço usado para abstrair sobre os tipos de spin. Para cada
/// tipo de spin, todos os estados possíveis são indexados com
/// um número inteiro cuja representação em binário representa
/// aquele estado. Por exemplo, para spin 1/2, 0 representa o
/// spin para baixo de 1, para cima. Logo o inteiro 0 indexa
/// o estado de spin para baixo e o inteiro 1, de spin para cima.
/// Para spin 1, 0 = 00 representa o spin -, 1 = 01, o spin 0 e
/// 2 = 10 o spin +. Note que não há nenhum estado correspondente
/// a 3 = 11 para spin um.
///
/// Em princípio é possível definir outras representações para um
/// mesmo "spin" definindo um novo tipo e implementando Spin para
/// este tipo. Por exemplo, usando complementos de dois, poderíamos
/// ter 00 = 0, 01 = + e 11 = - para um sistema de spin um. O exemplo
/// anterior, então, corresponderia a uma representação de um sistema
/// genérico de três estados. 
///
/// Todas as operações com spins são realizadas com base nos índices
/// dos estados possíveis. Isto evita ter que lidar com vetores de 
/// bits --- Vec<bool> --- que, acredito, diminuiriam a eficiência.
/// Note a exigência de TryFrom<u8>. 
pub trait Spin: Copy + TryFrom<State, Error = SpinError> {
    const BIT_SIZE: u8;
    const STATES: u8;
    const SPIN: &'static str;

    // Manter State direto ou voltar para Self?
    const MIN: State;
    const MAX: State;

    // Retorna o índice númerico do estado de spin `self`.
    fn index(&self) -> State;

    // Dado um índice
    fn raise(state: State) -> SpinResult<Option<State>>;

    fn lower(state: State) -> SpinResult<Option<State>>;
}


#[derive(Debug, PartialEq, Clone, Copy)]
pub enum SpinHalf {
    Down,
    Up,
}

impl TryFrom<State> for SpinHalf {
    type Error = SpinError;

    fn try_from(value: State) -> Result<Self, Self::Error>
    {
        if value == 0
        {
            Ok(Self::Down)
        }
        else if value == 1
        {
            Ok(Self::Up)
        }
        else
        {
            Err(SpinError::InvalidSiteState(value, Self::SPIN, Self::BIT_SIZE))
        }
    }
}

impl Spin for SpinHalf {
    const BIT_SIZE: u8 = 1;
    const STATES: u8 = 2;
    const SPIN: &'static str = "1/2";

    const MIN: State = 0;
    const MAX: State = 1;

    fn index(&self) -> State
    {
        match &self
        {
            Self::Down => 0,
            Self::Up   => 1,
        }
    }

    fn raise(state: State) -> SpinResult<Option<State>>
    {
        if state == 0 
        {
            Ok(Some(1))
        }
        else if state == 1
        {
            Ok(None)
        }
        else
        {
            Err(SpinError::InvalidSiteState(state, Self::SPIN, Self::BIT_SIZE))
        }
    }

    fn lower(state: State) -> SpinResult<Option<State>>
    {
        if state == 0 
        {
            Ok(None)
        }
        else if state == 1
        {
            Ok(Some(0))
        }
        else
        {
            Err(SpinError::InvalidSiteState(state, Self::SPIN, Self::BIT_SIZE))
        }
    }
}


#[derive(Debug, PartialEq, Clone, Copy)]
pub enum SpinOne {
    Plus,
    Zero,
    Minus,
}


impl TryFrom<State> for SpinOne {
    type Error = SpinError;

    fn try_from(value: State) -> Result<Self, Self::Error>
    {
        if value == 0
        {
            Ok(Self::Minus)
        }
        else if value == 1
        {
            Ok(Self::Zero)
        }
        else if value == 2
        {
            Ok(Self::Plus)
        }
        else
        {
            Err(SpinError::InvalidSiteState(value, Self::SPIN, Self::BIT_SIZE))
        }
    }
}


impl Spin for SpinOne {
    const BIT_SIZE: u8 = 2;
    const STATES: u8 = 3;
    const SPIN: &'static str = "1";

    const MIN: State = 0;
    const MAX: State = 2;

    fn index(&self) -> State
    {
        match &self
        {
            Self::Minus => 0,
            Self::Zero  => 1,
            Self::Plus  => 2,
        }
    }

    fn raise(state: State) -> SpinResult<Option<State>>
    {
        if state == 0 
        {
            Ok(Some(1))
        }
        else if state == 1
        {
            Ok(Some(2))
        }
        else if state == 2
        {
            Ok(None)
        }
        else
        {
            Err(SpinError::InvalidSiteState(state, Self::SPIN, Self::BIT_SIZE))
        }
    }

    fn lower(state: State) -> SpinResult<Option<State>>
    {
        if state == 0 
        {
            Ok(None)
        }
        else if state == 1
        {
            Ok(Some(0))
        }
        else if state == 2
        {
            Ok(Some(1))
        }
        else
        {
            Err(SpinError::InvalidSiteState(state, Self::SPIN, Self::BIT_SIZE))
        }
    }
}
