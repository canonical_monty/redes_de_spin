//! #Cadeias de spin
//!
//! Este módulo define a estrutura SpinChain que é usada para amazenar o estado
//! de uma cadeia de spin em um dado momento. SpinChain é essencialmente um
//! envólucro sobre inteiros sem sinal. A ideia é usar a representação binária
//! do inteiro para guardar os estados da cadeia. Por exemplo: `5 = 0b00000101`
//! em binário, usando oito bits. Assim, `5` poderia representar uma cadeia de
//! oito spins meio no estado `|00000101>`. Em princípio, é possível usar a 
//! mesma lógica para spins maiores, por exemplo, para spin um, poderiamos usar
//! dois bits ao invés de um e a representação de complemento para dois para
//! termos os estados `0 = 00`, `1 = 01` e `-1 = 10`. Logo `5 = 0b00_00_01_01`
//! corresponde ao estado `|0011>` e `9 = 0b00_00_10_01` corresponde ao estado
//! `|00-11>` de uma cadei de spin um.


// A FAZER:
//
// [ ] - Documentação!
// [ ] - TESTES!!
// [ ] - Melhorar o teste para set. No momento não testa o caso de índice além dos limites.
// [-] - Fazer o iterador dos sítios da cadeia. Possivelmente implementar Index
// [x] - URGENTE: O tamanho da cadeia deve ser compatível com o tamanho do visor!
//       Se este não for o caso, algumas operações podem gerar pânico devido a 
//       transbordamento do tipo que armazena os estados.
// [ ] - SpinChain deveria conter uma referência para SpinChainVisor? Pode ajudar
//       mas adiciona mais uma camada de indireção e não acho que teremos múltiplas
//       cadeias simultaneamente.
// [ ] - Fazer uma versão de from_state_with_capacity que automaticamente trunca
//       o estado se ele exceder o tamanho passado?



// Módulos
pub(crate) mod storage;
pub(crate) mod chain_view;
pub(crate) mod chain_iterator;
pub(crate) mod state_iterator;

use crate::{
    spin::{
        State,
        Spin,
    },
    spin_chain::{
        storage::BitStorage,
        chain_view::{
            BoundaryCondition,
            SpinChainView,
        },
    },
    error::{
        SpinError,
        SpinResult,
    }
};

/// Cadeia de spin.
#[derive(Debug, Clone, Copy)]
pub struct SpinChain<E: BitStorage, B: BoundaryCondition, S: Spin> {
    view: SpinChainView<B, S>,
    state: E,
}

impl<E: BitStorage, B: BoundaryCondition, S: Spin> SpinChain<E, B, S> {

    /// Retorna o total de *sítios* de spin S que a cadeia pode armazenar.
    #[inline]
    fn max_size() -> u8
    {
        E::WIDTH / S::BIT_SIZE
    }

    /// Inicia uma cadeia com capacidade máxima no estado `0 = |00...0>`.
    pub fn new() -> Self 
    {
        let view = SpinChainView::<B, S>::new(Self::max_size());
        Self { view, state: E::from(0_u8) }
    }

    /// Inicia uma cadeia com capacidade máxima no estado `state`. Verifica
    /// se o estado `state` é compatível com o tipo de cadeia. 
    ///
    /// Se `state` ultrapassa o tamanho máximo, retorna `SpinError::ChainOverflow`.
    /// Se contém algum estado de spin inválido retorna `SpinError::InvalidSiteState`.
    pub fn from_state(state: E) -> SpinResult<Self>
    {
        let view = SpinChainView::<B, S>::new(Self::max_size());
        match view.verify_chain_state(&state) 
        {
            Err(e) => Err(e),
            Ok(()) => Ok(Self { view, state }),
        }
    }

    /// Inicia uma cadeia com capacidade de `size` sítios no estado `0 = |0..0>`.
    ///
    /// Se `size * S::BIT_SIZE`, o número de bits necessários para guardar `size`
    /// sítios de spin `S``ultrapassar o tamanho máximo, retorna um erro:
    /// `SpinError::StorageOverflow`.
    pub fn with_capacity(size: u8) -> SpinResult<Self>
    {
        if size > Self::max_size()
        { 
            Err(SpinError::StorageOverflow(size * S::BIT_SIZE, E::TN, E::WIDTH)) 
        }
        else
        {
            let view = SpinChainView::<B, S>::new(size);
            Ok(Self { view, state: E::from(0_u8) })
        }
    }

    /// Inicia uma cadeia com capacidade de `size` sítios no estado `state`. 
    ///
    /// Se `size * S::BIT_SIZE`, o número de bits necessários para guardar 
    /// `size` sítios de spin `S``ultrapassar o tamanho máximo, retorna um
    /// erro `SpinError::StorageOverflow`. Se o estado `state` ultrapassar
    /// `size`, isto é, contiver mais sítios que `size`, retorna
    /// `SpinError::ChainOverflow` e se `state` possui ao menos um sítio
    /// em um estado inválido, retorna `SpinError::InvalidSiteState`.
    pub fn from_state_with_capacity(size: u8, state: E) -> SpinResult<Self>
    {
        if size > Self::max_size()
        { 
            Err(SpinError::StorageOverflow(size * S::BIT_SIZE, E::TN, E::WIDTH)) 
        }
        else
        {
            let view = SpinChainView::<B, S>::new(size);
            match view.verify_chain_state(&state) 
            {
                Err(e) => Err(e),
                Ok(()) => Ok(Self { view, state }),
            }
        }
    }

    /// Retorna o número de *sítios* da cadeia.
    #[inline]
    pub fn size(&self) -> u8
    {
        self.view.size()
    }

    /// Retorna uma referência ao estado atual da cadeia.
    #[inline]
    pub fn state(&self) -> &E
    {
        &self.state
    }

    /// Retorna uma referência mutável ao estado atual da cadeia.
    #[inline]
    pub fn state_mut(&mut self) -> &mut E
    {
        &mut self.state
    }

    
    /// Retorna o índice do estado do sítio index envolto por Option.
    /// Retorna None se o index é maior que o tamanho da cadeia.
    pub fn get(&self, index: u8) -> Option<u8>
    {
        self.view.get(&self.state, index)
    }


    /// Atribui o estado state ao sítio de índice index.
    pub fn set(&mut self, index: u8, site_state: State) -> SpinResult<()>
    {
        match self.view.set(self.state, index, site_state) 
        {
            Err(e)        => Err(e),
            Ok(new_state) =>
            {
                self.state = new_state;
                Ok(())
            }
        }
    }

}


// Aliases para cadeias de spin
pub mod aliases {

    use crate::{
        spin_chain::{
            SpinChain,
            chain_view::{
                OpenBoundary,
                PeriodicBoundary,
            },
        },
        spin::{
            SpinHalf,
            SpinOne,
        },
    };


    // Spin 1/2
    pub type SpinHalfChain<E, B>            = SpinChain<E, B, SpinHalf>;

    pub type OpenSpinHalfChain<E>           = SpinHalfChain<E, OpenBoundary>;
    pub type SmallOpenSpinHalfChain         = OpenSpinHalfChain<u8>;
    pub type MediumOpenSpinHalfChain        = OpenSpinHalfChain<u16>;
    pub type LargeOpenSpinHalfChain         = OpenSpinHalfChain<u32>;
    pub type HugeOpenSpinHalfChain          = OpenSpinHalfChain<u64>;

    pub type PeriodicSpinHalfChain<E>       = SpinHalfChain<E, PeriodicBoundary>;
    pub type SmallPeriodicSpinHalfChain     = PeriodicSpinHalfChain<u8>;
    pub type MediumPeriodicSpinHalfChain    = PeriodicSpinHalfChain<u16>;
    pub type LargePeriodicSpinHalfChain     = PeriodicSpinHalfChain<u32>;
    pub type HugePeriodicSpinHalfChain      = PeriodicSpinHalfChain<u64>;

    // Spin 1
    pub type SpinOneChain<E, B>             = SpinChain<E, B, SpinOne>;

    pub type OpenSpinOneChain<E>            = SpinOneChain<E, OpenBoundary>;
    pub type SmallOpenSpinOneChain          = OpenSpinOneChain<u8>;
    pub type MediumOpenSpinOneChain         = OpenSpinOneChain<u16>;
    pub type LargeOpenSpinOneChain          = OpenSpinOneChain<u32>;
    pub type HugeOpenSpinOneChain           = OpenSpinOneChain<u64>;

    pub type PeriodicSpinOneChain<E>        = SpinOneChain<E, PeriodicBoundary>;
    pub type SmallPeriodicSpinOneChain      = PeriodicSpinOneChain<u8>;
    pub type MediumPeriodicSpinOneChain     = PeriodicSpinOneChain<u16>;
    pub type LargePeriodicSpinOneChain      = PeriodicSpinOneChain<u32>;
    pub type HugePeriodicSpinOneChain       = PeriodicSpinOneChain<u64>;
}



#[cfg(test)]
mod test {
    use super::*;
    use crate::spin::{SpinHalf, SpinOne};

    // A FAZER:
    //
    // [ ] - Testar os construtores. Especificamente, os erros de estados inválidos
    //       e de tamanhos inválidos.


    #[test]
    fn size_spin_half_chain()
    {
        let spin_half_chain = SpinChain::<u8, OpenBoundary, SpinHalf>::new();
        assert_eq!(spin_half_chain.size(), 8);

        let spin_half_chain = SpinChain::<u16, OpenBoundary, SpinHalf>::new();
        assert_eq!(spin_half_chain.size(), 16);

        let spin_half_chain = SpinChain::<u32, OpenBoundary, SpinHalf>::new();
        assert_eq!(spin_half_chain.size(), 32);

        let spin_half_chain = SpinChain::<u64, OpenBoundary, SpinHalf>::new();
        assert_eq!(spin_half_chain.size(), 64);


        let spin_half_chain = SpinChain::<u8, PeriodicBoundary, SpinHalf>::new();
        assert_eq!(spin_half_chain.size(), 8);

        let spin_half_chain = SpinChain::<u16, PeriodicBoundary, SpinHalf>::new();
        assert_eq!(spin_half_chain.size(), 16);

        let spin_half_chain = SpinChain::<u32, PeriodicBoundary, SpinHalf>::new();
        assert_eq!(spin_half_chain.size(), 32);

        let spin_half_chain = SpinChain::<u64, PeriodicBoundary, SpinHalf>::new();
        assert_eq!(spin_half_chain.size(), 64);
    }

    #[test]
    fn size_spin_one_chain()
    {
        let spin_one_chain = SpinChain::<u8, OpenBoundary, SpinOne>::new();
        assert_eq!(spin_one_chain.size(), 4);

        let spin_one_chain = SpinChain::<u16, OpenBoundary, SpinOne>::new();
        assert_eq!(spin_one_chain.size(), 8);

        let spin_one_chain = SpinChain::<u32, OpenBoundary, SpinOne>::new();
        assert_eq!(spin_one_chain.size(), 16);

        let spin_one_chain = SpinChain::<u64, OpenBoundary, SpinOne>::new();
        assert_eq!(spin_one_chain.size(), 32);


        let spin_one_chain = SpinChain::<u8, PeriodicBoundary, SpinOne>::new();
        assert_eq!(spin_one_chain.size(), 4);

        let spin_one_chain = SpinChain::<u16, PeriodicBoundary, SpinOne>::new();
        assert_eq!(spin_one_chain.size(), 8);

        let spin_one_chain = SpinChain::<u32, PeriodicBoundary, SpinOne>::new();
        assert_eq!(spin_one_chain.size(), 16);

        let spin_one_chain = SpinChain::<u64, PeriodicBoundary, SpinOne>::new();
        assert_eq!(spin_one_chain.size(), 32);
    }

    #[test]
    fn get_spin_half_state()
    {
        let spin_half_chain = SpinChain::<
            u8,
            OpenBoundary,
            SpinHalf
        >::from_state(0b1101_0011).unwrap();

        assert_eq!(spin_half_chain.get(0), Some(1));
        assert_eq!(spin_half_chain.get(1), Some(1));
        assert_eq!(spin_half_chain.get(2), Some(0));
        assert_eq!(spin_half_chain.get(3), Some(0));
        assert_eq!(spin_half_chain.get(4), Some(1));
        assert_eq!(spin_half_chain.get(5), Some(0));
        assert_eq!(spin_half_chain.get(6), Some(1));
        assert_eq!(spin_half_chain.get(7), Some(1));
        assert_eq!(spin_half_chain.get(8), None);
    }

    // #[test]
    // fn set_spin_half_state()
    // {
    //     let mut spin_half_chain = SpinChain::<
    //         u8, 
    //         OpenBoundary,
    //         SpinHalf
    //     >::from_state(0b11010011);

    //     spin_half_chain.set(2, 1);
    //     assert_eq!(spin_half_chain.state(), &0b11010111);
    // }


    // #[test]
    // fn get_spin_one_state()
    // {
    //     let spin_one_chain = SpinChain::<u8, SpinOne>::from_state(
    //         BoundaryCondition::Open,
    //         0b10010010,
    //     );

    //     assert_eq!(spin_one_chain.get(0), Some(2));
    //     assert_eq!(spin_one_chain.get(1), Some(0));
    //     assert_eq!(spin_one_chain.get(2), Some(1));
    //     assert_eq!(spin_one_chain.get(3), Some(2));
    //     assert_eq!(spin_one_chain.get(4), None);
    // }

    // #[test]
    // fn set_spin_one_state()
    // {
    //     let mut spin_one_chain = SpinChain::<u8, SpinOne>::from_state(
    //         BoundaryCondition::Open,
    //         0b10010010,
    //     );

    //     spin_one_chain.set(1, 1);
    //     assert_eq!(spin_one_chain.state(), &0b10010110);
    // }
}

