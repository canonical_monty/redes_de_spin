//! # Iterador sobre os sítios de uma cadeia.
//!
//! Define `ChainIterator` que permitem iterar sobre os sítios de
//! uma cadeia no estado `E: BitStorage`. Assim, é possível expor
//! um API que trata uma cadeia de spins como se fosse, realmente,
//! um vetor de estados de cada sítio. A exceção é que, eu acho,
//! é impossível definir um iterador mutável porque isso exigiria
//! expor uma região da memória que é menor que um byte, o que é
//! impossível?? Se não fosse, essa biblioteca seria desnecessária.


// A FAZER:
// [ ] - Decidir o que fazer com condições de contorno periódicas
// [ ] - Testar condições de contorno periódicas


use crate::{
    spin_chain::{
        storage::BitStorage,
        chain_view::BoundaryCondition,
        SpinChain,
    },
    spin::Spin,
};


/// Iterador imutável sobre os sítios de uma cadeia de spins.
#[derive(Debug)]
pub struct ChainIterator<'a, E, B, S> where
    E: BitStorage,
    B: BoundaryCondition,
    S: Spin,
{
    /// Referência imutável à cadeia.
    chain: &'a SpinChain<E, B, S>,

    /// Posição do iterador na cadeia.
    position: Option<u8>,
}


impl<'a, E, B, S> Iterator for ChainIterator<'a, E, B, S> where
    E: BitStorage,
    B: BoundaryCondition,
    S: Spin,
{
    type Item = u8;

    fn next(&mut self) -> Option<Self::Item>
    {
        match self.position 
        {
            None => 
            {
                self.position = Some(0);
                self.chain.get(0)
            }
            Some(index) =>
            {
                self.position = Some(index + 1);
                self.chain.get(index + 1)
            }
        }
    }
}


impl<'a, E, B, S> IntoIterator for &'a SpinChain<E, B, S> where
    E: BitStorage,
    B: BoundaryCondition,
    S: Spin,
{
    type Item = u8;
    type IntoIter = ChainIterator<'a, E, B, S>;

    fn into_iter(self) -> Self::IntoIter 
    {
        ChainIterator { chain: self, position: None }
    }
}




#[cfg(test)]
mod test {
    use crate::{
        spin_chain::{
            SpinChain,
            chain_view::{
                OpenBoundary,
                PeriodicBoundary,
            },
        },
        spin::{
            SpinHalf,
            SpinOne,
        }
    };


    #[test]
    fn spin_half_open_chain_iterator()
    {
        let spin_half_chain = SpinChain::<
            u8,
            OpenBoundary,
            SpinHalf
        >::from_state(0b1101_0011).unwrap();

        let mut chain_iterator = (&spin_half_chain).into_iter();
        assert_eq!(chain_iterator.next(), Some(1));
        assert_eq!(chain_iterator.next(), Some(1));
        assert_eq!(chain_iterator.next(), Some(0));
        assert_eq!(chain_iterator.next(), Some(0));
        assert_eq!(chain_iterator.next(), Some(1));
        assert_eq!(chain_iterator.next(), Some(0));
        assert_eq!(chain_iterator.next(), Some(1));
        assert_eq!(chain_iterator.next(), Some(1));
        assert_eq!(chain_iterator.next(), None);
    }

    #[test]
    fn spin_one_opne_chain_iterator()
    {
        let spin_one_chain = SpinChain::<
            u8,
            OpenBoundary,
            SpinOne
        >::from_state(0b10_01_00_10).unwrap();

        let mut chain_iterator = (&spin_one_chain).into_iter();
        assert_eq!(chain_iterator.next(), Some(2));
        assert_eq!(chain_iterator.next(), Some(0));
        assert_eq!(chain_iterator.next(), Some(1));
        assert_eq!(chain_iterator.next(), Some(2));
        assert_eq!(chain_iterator.next(), None);
    }
}
