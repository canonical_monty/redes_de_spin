//! # Visor de Cadeias de Spin
//!
//! Define `SpinChainView` que serve como molde para uma dada cadeia de spins
//! e permite interpretar um certo estado `E: BitStorage` como um estado da
//! cadeia modelada por `SpinChainView`.
//!
//! Por exemplo, considere um estado armazenado em um `u8`: 9 = 0b00001001.
//! Para saber o que os bits estão representando é necessário conhecer a 
//! cadeia de spins cujo estado é representado por este inteiro. Se a cadeia
//! é uma cadeia de spins = 1 com 4 sítios, então `9 = | - - + 0 >`. Se for uma
//! cadeia de spins = 1/2 com 8 sítios, então `9 = | - - - - + - - + >`.
//!

// A FAZER:
// [x] Transfomar BoundaryCondition em um traço e criar dois tipos OpenBoundary
//     e PeriodicBoundary.
//
// [ ] Decidir se get_unchecked e get deveriam receber uma referência mutável.
// [x] Verificar a questão do pânico em get para o caso de atribuição de um
//     estado que seja inválido para o tipo de cadeia em questão.
// 
// [ ] Decidir o que fazer com BoundaryCondition. Manter como um traço ou 
//     voltar para um enum. Ou os dois???


use std::{
    marker::PhantomData,
};

use crate::{
    error::{
        SpinError,
        SpinResult,
    },
    spin::{
        Spin,
        State,
    },
    spin_chain::storage::BitStorage,
};



/// Traço usado para indicar a condição de contorno da cadeia.
pub trait BoundaryCondition: Clone + Copy {
    const TYPE: &'static str;

    fn boundary_type(&self) -> &'static str
    {
        Self::TYPE
    }
}

/// Condição de contorno aberta.
#[derive(Debug, Clone, Copy)]
pub struct OpenBoundary;

impl BoundaryCondition for OpenBoundary {
    const TYPE: &'static str = "Aberta";
}


/// Condição de contorno periódica.
#[derive(Debug, Clone, Copy)]
pub struct PeriodicBoundary;

impl BoundaryCondition for PeriodicBoundary {
    const TYPE: &'static str = "Periódica";
}




/// Visor de cadeia de spins
#[derive(Debug, Clone, Copy)]
pub struct SpinChainView<B: BoundaryCondition, S: Spin> {
    /// Número de *sítios* da cadeia.
    ///
    /// Dependendo do valor de `Spin` um sítio pode exigir um ou mais bits.
    size: u8,

    /// Condições de contorno da cadeia.
    boundary: PhantomData<B>,

    /// Tipo de spin das partículas da cadeia.
    spin: PhantomData<S>,
}


impl<B: BoundaryCondition, S: Spin> SpinChainView<B, S> {


    /// Constroi um visor com `size` sítios e condição de contorno `boundary`.
    #[inline]
    pub fn new(size: u8) -> Self
    {
        Self { size, boundary: PhantomData, spin: PhantomData }
    }

    /// Retorna o tamanho --- número de sítios --- da cadeia. Este valor é
    /// diferente do número de bits da cadeia no caso de spins maiores que 1/2.
    #[inline]
    pub fn size(&self) -> u8
    {
        self.size
    }

    /// Verifica se `chain` é um estado compatível com `self`. Isto é, 
    /// verifica se `chain` não contêm mais sítios que `self` nem contém
    /// estados de spin inválidos. 
    ///
    /// Retorna `SpinError::InvalidSiteState` se o estado da cadeia contem
    /// algum sitio com estado inválido e `SpinError::ChainOverflow` se a 
    /// cadeia contêm mais sítios que o visor. A cadeia conter mais sitios
    /// que o visor não é um problema em si e não altera o funcionamento,
    /// mas, se algum desses estados não estiver no estado `0`, isto altera
    /// o inteiro que representa o estado, o que pode ser indesejado.
    pub fn verify_chain_state<E: BitStorage>(&self, chain: &E) -> SpinResult<()>
    {
        let mut mask = E::from(0_u8);
        for site in 0..self.size
        {
            // Verifica se possui algum estado inválido
            let site_state = self.get(chain, site).unwrap();
            if let Err(_) = S::try_from(site_state)
            {
                return Err(SpinError::InvalidSiteState(site_state, S::SPIN, S::BIT_SIZE));
            }

            // Máscara para verificar o tamanho
            for i in 0..S::BIT_SIZE
            {
                mask |= E::from(1_u8) << site * S::BIT_SIZE + i;
            }
        }

        // Verifica o tamanho
        if *chain & !mask != E::from(0_u8)
        {
            let chain_str = format!("{:0b}", chain);
            return Err(SpinError::ChainOverflow(chain_str, self.size));
        }

        Ok(())
    }

    /// Dado uma cadeia em um certo estado, `chain`, retorna o estado de 
    /// spin do sítio `site`, interpretando a cadeia segundo os parâmetros
    /// do visor: tipo de spin e tamanho. Retorna None se o `site` está
    /// parcial ou totalmente fora dos limites da cadeia.
    #[inline]
    pub fn get<E: BitStorage>(&self, chain: &E, site: u8) -> Option<State>
    {
        if site >= self.size
        {
            None
        }
        else
        {
            let mut state: State = 0;
            for i in 0..S::BIT_SIZE
            {
                let mask = E::from(1_u8) << i + (site * S::BIT_SIZE);
                if *chain & mask != E::from(0_u8)
                {
                    state += 1_u8 << i;
                }
            }

            Some(state)
        }
    }

    /// Dado uma cadeia em um certo estado, `chain`, retorna uma nova cadeia
    /// onde o estado do sítio `site` foi alterado para `state`. A cadeia é
    /// interpretada segundo o visor: tipo de spin e tamanho. Assume tacitamente
    /// que `state` é um estado compatível com o spin da cadeia. Se não for,
    /// o estado é truncado ou completado com zeros conforme for apropriado. 
    /// Retorna None se `site` está parcial ou totalmente fora dos limites da
    /// cadeia.
    pub fn set<E: BitStorage>(&self, chain: E, site: u8, state: State) -> SpinResult<E>
    {
        if site >= self.size
        {
            let err = SpinError::OutOfBounds(
                site * S::BIT_SIZE,
                self.size * S::BIT_SIZE
            );

            Err(err)
        }
        else
        {
            // Verifica se o estado é valido para o tipo de spin.
            if let Err(e) = S::try_from(state)
            {
                return Err(e);
            }

            // Constroi uma máscara com 1 apenas nos bits do sítio em questão.
            let mut mask = E::from(0_u8);
            let bit_position = site * S::BIT_SIZE;
            for i in 0..S::BIT_SIZE
            {
                mask |= E::from(1_u8) << i + bit_position;
            }

            // Desloca a sequência de bits do estado até o índice em questão:
            let shifted_state = E::from(state) << bit_position;

            // Retorna o novo estado
            Ok((chain & !mask) | shifted_state)
        }
    }


    /// Avança o estado do sítio `site` de `chain` em um. Retorna o novo 
    /// estado da cadeia e um `bool` indicando se o estado do sítio era
    /// o estado máximo permitido ou não. 
    ///
    /// Em termos físicos, funciona como exatamente como o operador de 
    /// levantamento aplicado no sítio `site`. Mas se o estado do sítio
    /// já era o estado máximo, o resultado é o menor estado possível.
    /// Por exemplo, se o sítio estava no estado de spin meio, `|+>, então
    /// o estado resultando é `|->. E o resultado da função é este novo
    /// estado junto com `true`.
    ///
    /// O booleano é essencialmente para indicar se 'vai um' e o estado
    /// do sítio seguinte deveria ser aumentado. Usado no iterador de
    /// estados em [state_iterator](state_iterator).
    pub fn raise_site<E: BitStorage>(
        &self,
        chain: E,
        site: u8
    ) -> SpinResult<(E, bool)>
    {
        if site >= self.size
        {
            let err = SpinError::OutOfBounds(
                site * S::BIT_SIZE,
                self.size * S::BIT_SIZE
            );

            Err(err)
        }
        else
        {
            // Sabemos que o índice está dentro dos limites.
            let mut site_state = self.get(&chain, site).unwrap();
            let mut carry = false;

            match S::raise(site_state.into())
            {
                Err(e)                  => return Err(e),
                Ok(Some(raised_state))  => site_state = raised_state,
                Ok(None)                => 
                {
                    carry = true;
                    site_state = S::MIN;
                }
            }

            // Sabemos que o `index` e `site_state` são válidos.
            let new_state = self.set(chain, site, site_state).unwrap();  
            Ok((new_state, carry))
        }
    }
}
