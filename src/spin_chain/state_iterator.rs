//! # Iterador de estados 
//!
//! Define `StateIterator` que itera sobre todos os estados válidos
//! de uma cadeia.
//!
//! Uma das vantagens de armazenar spins na representação em binário
//! de um inteiro é que, no caso de spins semi-inteiros, podemos 
//! iterar sobre todos os estados possíveis da cadeia simplesmente 
//! contando de 0 a 2^(L) - 1 onde L é o tamanho da cadeia. Para spins 
//! inteiros alguns interios contêm sequências de bits que não 
//! representam estado algum. Este iterador produz, em ordem crescente,
//! todos os estados válidos de uma cadeia, independentemente do spin.

// A FAZER:
//
// [ ] - Decidir se StateIterator e, por extensão, SpinChain deveriam
//       usar apenas uma referência a um SpinChainView. 



use crate::{
    spin_chain::{
        SpinChain,
        chain_view::{
            BoundaryCondition,
            SpinChainView,
        },
        storage::BitStorage,
    },
    spin::Spin,
};


/// Enum caracterizando o estado do iterador.
#[derive(Debug)]
enum IteratorState {
    Unitialized,
    Initialized,
    Finished,
}


/// Iterador de estados.
#[derive(Debug)]
pub struct StateIterator<E, B, S> where
    E: BitStorage,
    B: BoundaryCondition,
    S: Spin,
{
    /// Estado interno do iterador
    state: IteratorState,

    /// Visor de cadeia de spins.
    chain_view: SpinChainView<B, S>,

    /// Estado atual da cadeia.
    chain_state: Option<E>,
}

impl<E, B, S> StateIterator<E, B, S> where
    E: BitStorage,
    B: BoundaryCondition,
    S: Spin,
{

    pub fn new(chain_view: SpinChainView<B, S>) -> Self
    {
        Self {
            state: IteratorState::Unitialized,
            chain_view,
            chain_state: None
        }
    }
}


impl<E, B, S> Iterator for StateIterator<E, B, S> where 
    E: BitStorage,
    B: BoundaryCondition,
    S: Spin,
{
    type Item = SpinChain<E, B, S>;

    fn next(&mut self) -> Option<Self::Item>
    {
        match self.state
        {
            IteratorState::Unitialized => 
            {
                self.state = IteratorState::Initialized;
                self.chain_state = Some(E::from(0_u8));

                Some(SpinChain{ view: self.chain_view, state: E::from(0_u8) })
            }
            IteratorState::Initialized =>
            {
                let mut finished = true;
                let mut state = self.chain_state.unwrap();

                for i in 0..self.chain_view.size()
                {
                    // Assumindo que E::from(0_u8) é sempre um estado válido.
                    let (new_state, carry) = self.chain_view
                        .raise_site(state, i)
                        .unwrap();

                    state = new_state;
                    if !carry
                    {
                        finished = false;
                        break;
                    }
                }

                if finished
                {
                    self.chain_state = None;
                    self.state = IteratorState::Finished;

                    return None;
                }

                self.chain_state = Some(state);
                Some(SpinChain{ view: self.chain_view, state })
            }
            IteratorState::Finished =>
            {
                None
            }
        }
    }
}


#[cfg(test)]
mod test {
    use super::*;
    use crate::{
        spin_chain::chain_view::{
            OpenBoundary,
            PeriodicBoundary,
        },
        spin::{
            SpinHalf,
            SpinOne
        },
    };

    // A FAZER:
    //
    // [ ] - Melhorar o test do iterador de spin 1/2: comparar manualmente
    //       ao invés de em um loop.
    //
    // [ ] - Fazer o teste do iterador de spin 1

    #[test]
    fn spin_half_iteration()
    {
        let spin_half_view = SpinChainView::<OpenBoundary, SpinHalf>::new(8);
        let mut iterator = StateIterator::<
            u8,
            OpenBoundary,
            SpinHalf
        >::new(spin_half_view);

        let mut comp_state = 0_u8;
        while let Some(chain_state) = iterator.next()
        {
            println!("{:?}", chain_state);
            assert_eq!(chain_state.state(), &comp_state);
            if comp_state != 255
            {
                comp_state += 1;
            }
        }
    }


}
