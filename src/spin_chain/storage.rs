//! # Amazem de cadeias
//!
//! Define o traço `BitStorage` que marca um os inteiros sem sinal --- exceto
//! `u128` --- como tipos adequados para guardar estados de cadeias de spin na
//! sua representação binária. Todos os tipos que implementa `BitStorage` 
//! implementam operações bit-a-bit: `!`, `&`, `&=`, `|` e `|=`. Além disso, eles
//! podem ser "indexados" por um inteiro `u8`, implentando `<< u8` e `>> u8` além
//! de implementarem `From<u8>`.

use std::{
    ops::{
        Not,
        BitAnd,
        BitAndAssign,
        BitOr,
        BitOrAssign,
        // ? BitXor,
        // ? BitXorAssign,
        Shl,
        ShlAssign,
        Shr,
        ShrAssign,
    },
    fmt::{
        Debug,
        Display,
        Binary,
    },
};

/// Um traço que caracteriza tipos que podem ser usados como armazem
/// direto de bits.
///
/// Este traço é implementado apenas para inteiros sem sinal.
///
/// A dependência com Seal impede que este traço seja implementado
/// por caixas que usem esta biblioteca. 
///
/// Agradecimento a `bitvec` por inspirar este código.

// Qualquer tipo que vá ser usado como `BitStorage` deve implementar
// Binary, para ser exibido em binário, além de operações bit-a-bit:
// Not, And, Or. Além disso, deve implementar `>>` e `<<`. Em particular,
// para estes são implementados para índices `u8` já que esse tipo é
// mais que suficiente para guardar o índice de bits de qualquer um dos
// outros tipos. 
//
//  -- Isto é, um `BitStorage` `u8` tem 8 bits. Ou seja, o índice de bits
//  vai de 0 a 7. Similarmente `u32` tem 32 bits e assim por diante. Todos
//  cabem dentro de um `u8`. Em particular, se, eventualmente, eu extender
//  as cadeias para aceitarem `Vec<E: BitStorage>`, então os índices podem
//  ser (usize, u8) onde `usize` indexa o vetor e `u8` o bit.
//
// Também deve implementar From<u8>, isto permite criar máscaras facilmente
// ao invés de necessitar dos traços One e Zero de versões anteriores.
pub trait BitStorage:
    Seal 
    + Sized

    // Binário
    + Binary
    + BitAnd<Self, Output=Self>
    + BitAndAssign<Self>
    + BitOr<Self, Output=Self>
    + BitOrAssign<Self>
    + Not<Output=Self>
    
    // Máscaras
    + From<u8>
    + Shl<u8, Output=Self>
    + ShlAssign<u8>
    + Shr<u8, Output=Self>
    + ShrAssign<u8>

    // Outros
    + Copy
    + Debug
    + Display
    + Eq
{
    /// Tamanho, em bits, do tipo.
    const WIDTH: u8 = 1 << Self::BITS;

    /// Número de bits necessários para indexar o tipo.
    const BITS: u8;

    /// Máscara de bits usadas para transformar um `usize` arbitrário
    /// em um índice de bits.
    const MASK: u8 = Self::WIDTH - 1;

    /// Guarda o nome do tipo primitivo. Usado para debugar.
    #[doc(hidden)]
    const TN: &'static str;


    /// Atribui o valor `value` ao bit em `place`.
    #[inline]
    fn set(&mut self, place: u8, value: bool)
    {
        assert!(place <= Self::MASK, "Índice fora dos limites");

        // Zera o bit selecionado
        *self &= !(Self::from(1_u8) << place);

        // Atribui o valor ao bit
        *self |= Self::from(value as u8) << place;
    }
    
    /// Consulta o valor do bit em `place`.
    #[inline]
    fn get(&self, place: u8) -> bool {
        assert!(place <= Self::MASK, "Índice fora dos limites");

        // Transforma o bit selecionado no bit menos significativo
        // e zera todos os outros.
        (*self >> place) & Self::from(1_u8) == Self::from(1_u8)
    }
    
    /// Conta o número de bits com valor 1.
    fn ones(&self) -> u32;

    /// Conta o número de bits com valor 0.
    fn zeros(&self) -> u32;
}


// Implementações 
impl BitStorage for u8 {
    const BITS: u8 = 3;
    const TN: &'static str = "u8";

    fn ones(&self) -> u32 
    {
        self.count_ones()
    }

    fn zeros(&self) -> u32
    {
        self.count_zeros()
    }
}

impl BitStorage for u16 {
    const BITS: u8 = 4;
    const TN: &'static str = "u16";

    fn ones(&self) -> u32 
    {
        self.count_ones()
    }

    fn zeros(&self) -> u32
    {
        self.count_zeros()
    }
}

impl BitStorage for u32 {
    const BITS: u8 = 5;
    const TN: &'static str = "u32";

    fn ones(&self) -> u32 
    {
        self.count_ones()
    }

    fn zeros(&self) -> u32
    {
        self.count_zeros()
    }
}

// Há considerações de memória aqui. Se, na memória, u64 não for
// contíguo, isso pode levar a diminuição da performance devido
// ao tempo de acesso da memória.
impl BitStorage for u64 {
    const BITS: u8 = 6;
    const TN: &'static str = "u64";

    fn ones(&self) -> u32 
    {
        self.count_ones()
    }

    fn zeros(&self) -> u32
    {
        self.count_zeros()
    }
}



/// Traço para selar BitStorage 
///
/// O traço é público no módulo e outros módulos na caixa podem
/// usá-lo. Enquanto este traço não for exportado pela raiz da
/// caixa e este módulo for privado, então é impossível implementar
/// `BitStorage` fora dessa caixa. 
#[doc(hidden)]
pub trait Seal {}

impl Seal for u8 {}
impl Seal for u16 {}
impl Seal for u32 {}
impl Seal for u64 {}
